package com.orderendpoint.services;

import com.orderendpoint.entities.Order;
import com.orderendpoint.entities.Phone;
import com.orderendpoint.entities.PhoneList;

import org.springframework.stereotype.Service;





/**
 * Contains services for orders.
 */
@Service
public class OrderService {





    public boolean isOrderValid(final PhoneList phoneList,final Order order) {

       boolean result= order.getBuyCandidatePhoneList().
               stream().allMatch(x -> existGivenOrderPhoneInPhoneList(phoneList,x));
       return result;
    }

    private boolean existGivenOrderPhoneInPhoneList(final PhoneList phoneList,final Phone phone) {

        boolean result= phoneList.getPhoneList().stream().anyMatch(x -> x.getPhoneId().equalsIgnoreCase(phone.getPhoneId()));
        return result;
    }




}
