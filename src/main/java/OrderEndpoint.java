import com.fasterxml.jackson.databind.ObjectMapper;
import com.orderendpoint.entities.Order;
import com.orderendpoint.entities.PhoneList;
import com.orderendpoint.services.OrderService;
import com.orderendpoint.services.PhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;


@RestController
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.orderendpoint.services" })
public class OrderEndpoint {

    private OrderService orderService;
    private PhoneService phoneService;

    @Autowired
    public void setOrderService(final OrderService orderService) {
        this.orderService = orderService;
    }
    @Autowired
    public void setPhoneService(final PhoneService phoneService) {
        this.phoneService = phoneService;
    }



    @RequestMapping(value = "/order", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createOrder(@Valid @RequestBody String order) throws IOException {

        ResponseEntity result;
        result = new ResponseEntity<>(HttpStatus.PRECONDITION_REQUIRED);

        ObjectMapper mapper=new ObjectMapper();

        final Order obj = mapper.readValue(order, Order.class);

        if (this.phoneService.retrievePhoneList().isPresent() ) {

            PhoneList phoneList = this.phoneService.retrievePhoneList().get();
            if (this.orderService.isOrderValid(phoneList, obj)) {

                BigDecimal totalOrderAmount = obj.getBuyCandidatePhoneList().stream().
                        map(phone -> phone.getPrice()).
                        reduce(BigDecimal.ZERO, BigDecimal::add);

                System.out.println("####ORDER SUCCESSFULLY CREATED####");
                System.out.println("NAME: " + obj.getCustomer().getName());
                System.out.println("SURNAME:" + obj.getCustomer().getSurname());
                System.out.println("EMAIL:" + obj.getCustomer().getEmail());
                System.out.println("PHONES: " + obj.getBuyCandidatePhoneList().toString());
                System.out.println("TOTAL ORDER AMOUNT: " + String.valueOf(totalOrderAmount));
                result = new ResponseEntity<>(phoneList, HttpStatus.OK);

            } else {
                System.out.println("####INVALID ORDER####");
                result = new ResponseEntity<>(phoneList, HttpStatus.BAD_REQUEST);


            }
        }
        return result;
    }



    public static void main(String[] args) {
        SpringApplication.run(OrderEndpoint.class, args);
    }

}