package com.orderendpoint.utils;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


import java.io.IOException;

public class HttpUtils {



    public static Response get(final String url) throws IOException {


        Request request = new Request.Builder().url(url).build();
        Response response = null;
        try {
            response = new OkHttpClient().newCall(request).execute();


        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;


    }


}
