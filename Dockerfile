FROM openjdk:10-jre-slim
COPY ./target/orderService-LATEST-SNAPSHOT.jar /usr/src/orderService/
WORKDIR /usr/src/orderService
EXPOSE 8080
CMD ["java", "-jar", "orderService-LATEST-SNAPSHOT.jar"]