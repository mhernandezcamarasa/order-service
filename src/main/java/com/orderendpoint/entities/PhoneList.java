package com.orderendpoint.entities;

import lombok.*;

import java.io.Serializable;
import java.util.List;


/**
 * Representation of phone data
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PhoneList implements Serializable {

    private static final long serialVersionUID = 638808919338124787L;
    private List<Phone> phoneList;


}
