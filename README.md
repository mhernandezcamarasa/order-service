** Deployment instructions **

Provided latest releases of maven and docker are installed, and also that we are in the ROOT directory of the project:

1. mvn clean install (In order to generate orderService-LATEST-SNAPSHOT.jar)

2. docker build -t orderservice-manual-build . (In order to create docker image)

3. docker run --network="host" -p 8002:8080 orderservice-manual-build (In order to start docker container using host network,otherwise we can't communicate with phone service inside this container)

4. In order to test this service :
         1. Install Postman in your machine
         2. Create a POST request  http://localhost:8080/order
         3. Use this JSON object in the body to test the "happy path" :
            {"customer": {"name": "mario","surname": "mario","email": "mario@mario"},
             "buyCandidatePhoneList":
             [
             {"phoneId":"0001","image":"http://mydomain/phone1Image.png","name":"iphone","description":"iphone","price":1000},
             {"phoneId":"0002","image":"http://mydomain/phone2Image.png","name":"iphone","description":"iphone","price":800},
             {"phoneId":"0003","image":"http://mydomain/phone3Image.png","name":"iphone","description":"iphone","price":150},
             {"phoneId":"0004","image":"http://mydomain/phone4Image.png","name":"iphone","description":"iphone","price":180}
             ]
            } 
           
           Or this one to test the "unhappy path"

           {"customer": {"name": "mario","surname": "mario","email": "mario@mario"},
            "buyCandidatePhoneList":
             [
             {"phoneId":"0001","image":"http://mydomain/phone1Image.png","name":"iphone","description":"iphone","price":1000},
             {"phoneId":"0002","image":"http://mydomain/phone2Image.png","name":"iphone","description":"iphone","price":800},
             {"phoneId":"0003","image":"http://mydomain/phone3Image.png","name":"iphone","description":"iphone","price":150},
             {"phoneId":"0005","image":"http://mydomain/phone4Image.png","name":"iphone","description":"iphone","price":180}
             ]
             }
     Required output will show up in springboot console
