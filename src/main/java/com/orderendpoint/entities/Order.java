package com.orderendpoint.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

import java.io.Serializable;
import java.util.List;


/**
 * Representation of order data
 */

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Order implements Serializable {

    private static final long serialVersionUID = 638808919338124787L;
    private Customer customer;
    private List<Phone> buyCandidatePhoneList;

}
