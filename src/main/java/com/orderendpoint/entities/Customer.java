package com.orderendpoint.entities;

import lombok.*;

import java.io.Serializable;


/**
 * Representation of phone data
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Customer implements Serializable {

    private static final long serialVersionUID = 638808919338124787L;
    private String name;
    private String surname;
    private String email;


}
