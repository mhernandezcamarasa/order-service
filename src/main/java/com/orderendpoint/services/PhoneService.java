package com.orderendpoint.services;

import com.orderendpoint.entities.Phone;
import com.orderendpoint.entities.PhoneList;
import com.orderendpoint.utils.HttpUtils;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;


import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;



/**
 * Contains services for phones.
 */
@Service
public class PhoneService {

    public static final String PHONE_SERVICE_ENDPOINT="http://127.0.0.1:8001/phone/";
    public static final String PHONE_SERVICE_LIST_METHOD="getList";

    public Optional<PhoneList> retrievePhoneList() {
        Optional<PhoneList> result=Optional.empty();

        try {
            Response response = HttpUtils.get(PHONE_SERVICE_ENDPOINT.concat(PHONE_SERVICE_LIST_METHOD));
            if (response.isSuccessful()) {

                result = getPhoneListFromJSON(result, response);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private Optional<PhoneList> getPhoneListFromJSON(Optional<PhoneList> result, Response response) throws IOException {
        String jsonData = response.body().string();
        JSONObject jobject = new JSONObject(jsonData);
        JSONArray jarray = jobject.getJSONArray("phoneList");

        List<Phone> phoneList=new ArrayList<>();

        for (int i = 0; i < jarray.length(); i++) {
            JSONObject jsonobject = jarray.getJSONObject(i);
            String phoneId= jsonobject.getString("phoneId");
            String description= jsonobject.getString("description");
            String name= jsonobject.getString("name");
            String image= jsonobject.getString("image");
            BigDecimal price = jsonobject.getBigDecimal("price");
            Phone phone = Phone.builder().name(name).phoneId(phoneId).description(description).
                          image(image).price(price).build();
            phoneList.add(phone);

        }

        result  = Optional.of(PhoneList.builder().phoneList(phoneList).build());
        return result;
    }




}
